//
//  MapViewModel.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/5/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation
import RxSwift
import CoreLocation
class MapViewModel : MapViewModelType, MapViewModelInput, MapViewModelOutput {
    
    var input: MapViewModelInput { return self }
    var output: MapViewModelOutput { return self }
    
    //input
    
    private var viewDidLoad = PublishSubject<Bool>()
    func didLoadView() {viewDidLoad.onNext(true)}
    
    private var viewLoading = PublishSubject<Bool>()
    func viewLoading(isLoading: Bool) {viewLoading.onNext(isLoading)}
    
    private var changeMode = PublishSubject<NavigationMode>()
    func changeMode(navigationModeOld: NavigationMode) {changeMode.onNext(navigationModeOld)}
    
    private var openDetail = PublishSubject<Post>()
    func openDetail(post: Post) {openDetail.onNext(post)}
    
    private var longPressed = PublishSubject<CLLocationCoordinate2D>()
    func longPressed(coordinates: CLLocationCoordinate2D) {longPressed.onNext(coordinates)}
    
    private var  takePhotoButtonPressed = PublishSubject<CLLocation>()
    func takePhotoButtonPressed(coordinates: CLLocation) { takePhotoButtonPressed.onNext(coordinates)}
    
    //output
    
    var navigationModeChanged: Observable<NavigationMode>
    var uploadListOfPosts: Observable<[Post]>
    var longPressLocation: Observable<CLLocation>
    var locationForPhoto: Observable<CLLocation>
    var openDetailScreen: Observable<Post>
    var loadView: Observable<Bool>
    
    init() {
        
        uploadListOfPosts = viewDidLoad.flatMap{_ in
              FBClient.client.retriveListOfPosts()}
            .map { posts in
                let categories = SelectedCategory.categories
                var natureCategory = SelectedCategory(category: CategoryType.NATURE, isSelected: false)
                var friendsCategory = SelectedCategory(category: CategoryType.FRIENDS, isSelected: false)
                var defaultCategory = SelectedCategory(category: CategoryType.DEFAULT, isSelected: false)
                for item in categories{
                    if item.categoryType.rawValue == 1 {friendsCategory = item}
                    if item.categoryType.rawValue == 2 {natureCategory = item}
                    if item.categoryType.rawValue == 3 {defaultCategory = item}
                }
                var listPosts = [Post]()
                for post in posts {
                    switch(post.category){
                    case CategoryType.DEFAULT.rawValue:
                            if defaultCategory.isSelected { listPosts.append(post)}
                        case CategoryType.FRIENDS.rawValue:
                            if friendsCategory.isSelected { listPosts.append(post)}
                        case CategoryType.NATURE.rawValue:
                            if natureCategory.isSelected { listPosts.append(post)}
                        default:
                            return posts
                    }
                }
                return listPosts
        }
        
        openDetailScreen = openDetail.debounce(3.0, scheduler: MainScheduler.instance).map{ post in
                return post
        }
        
        longPressLocation = longPressed.map{ loaction in
            return CLLocation(latitude: loaction.latitude, longitude: loaction.longitude)
        }
        
        locationForPhoto = takePhotoButtonPressed.map{ loaction in
            return  loaction
        }
        
        navigationModeChanged = changeMode.map{ oldMode in
            if oldMode == NavigationMode.FOLLOW {
                return NavigationMode.DISCOVER
            } else {
                return NavigationMode.FOLLOW
            }
        }
        
        loadView = viewLoading.map{ isLoading in
            return !isLoading
        }
        
        
    }
    
}

