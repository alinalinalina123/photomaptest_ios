        //
//  MapViewController.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/2/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import RxSwift
        
class MapViewController: UIViewController, UIGestureRecognizerDelegate, DetailMapViewDelegate  {
    
    @IBOutlet weak var modeButton: UIButton!
    @IBOutlet weak var progressBar: UIActivityIndicatorView!
    @IBOutlet weak var mapView: MKMapView!
    var locationManager:CLLocationManager!
    let picker = UIImagePickerController()
    var pinAnnotationView:MKPinAnnotationView!
    var selected: CustomCallOutView!
    var location: CLLocation?
    var categories = SelectedCategory.categories
    var viewModel: MapViewModel!
    let bag = DisposeBag()
    let navigation = HomeNavigationController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initMapView()
        progressBar.isHidden = true
    }

    func gestureRecognizer(_: UIGestureRecognizer,shouldRecognizeSimultaneouslyWith shouldRecognizeSimultaneouslyWithGestureRecognizer:UIGestureRecognizer) -> Bool {
        return true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        viewModel = MapViewModel()
       
        
        viewModel.output.loadView
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { isLoading in
                self.progressBar.isHidden = isLoading
                self.mapView.isUserInteractionEnabled = isLoading
            }).disposed(by: bag)
        
        viewModel.output.longPressLocation
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { location in
                AppAlert.actionSheetPhoto(selfView: self, userLocation: location)
            }).disposed(by: bag)
        
        viewModel.output.openDetailScreen
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { post in
                self.viewModel.input.viewLoading(isLoading: false)
                self.navigation.navigateToDetailPhoto(controller: self, post: post)
            }).disposed(by: bag)
        
        viewModel.output.locationForPhoto
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { location in
                AppAlert.actionSheetPhoto(selfView: self, userLocation: self.locationManager.location!)
            }).disposed(by: bag)
        
        viewModel.output.navigationModeChanged
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { mode in
                SelectedNavigation.modeNavigation = mode
                self.changeModeNavigation()
            }).disposed(by: bag)
        
        viewModel.output.uploadListOfPosts
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { post in
                let allAnnotations = self.mapView.annotations
                self.mapView.removeAnnotations(allAnnotations)
                self.viewModel.input.viewLoading(isLoading: false)
                for item in post{
                    let annotation = CustomAnnotation(post: item)
                    self.pinAnnotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "pin")
                    self.mapView.addAnnotation(self.pinAnnotationView.annotation!)
                }
               
            }).disposed(by: bag)
        
         viewModel.input.didLoadView()
         viewModel.input.viewLoading(isLoading: true)
    }
    
    func detailsRequestedForPost(post: Post) {
        viewModel.input.viewLoading(isLoading: true)
        viewModel.input.openDetail(post: post)
    }

    @IBAction func takePhoto(_ sender: Any) {
        if self.locationManager.location != nil {
           viewModel.input.takePhotoButtonPressed(coordinates: self.locationManager.location!)
        }
    }

    @IBAction func changeMode(_ sender: Any) {
        viewModel.input.changeMode(navigationModeOld: SelectedNavigation.modeNavigation)
    }
    
    @IBAction func handleLongPress(_ sender: UILongPressGestureRecognizer) {
        viewModel.input.longPressed(coordinates:  mapView.convert(sender.location(in: mapView), toCoordinateFrom: mapView))
    }
 
    @IBAction func rotateMap(_ sender: UIRotationGestureRecognizer) {
        if(sender.state == .ended) {
            viewModel.input.changeMode(navigationModeOld: NavigationMode.FOLLOW)
        }
    }
    
    @IBAction func dragMap(_ sender: UIPanGestureRecognizer) {
        viewModel.input.changeMode(navigationModeOld: NavigationMode.FOLLOW)
    }
    
}
