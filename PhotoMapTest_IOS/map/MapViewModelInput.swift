//
//  MapViewModelInput.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/5/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation
import MapKit
import CoreLocation
protocol MapViewModelInput {
    func didLoadView()
    func longPressed(coordinates: CLLocationCoordinate2D)
    func takePhotoButtonPressed(coordinates: CLLocation)
    func openDetail(post: Post)
    func changeMode(navigationModeOld: NavigationMode)
    func viewLoading(isLoading: Bool)
}
