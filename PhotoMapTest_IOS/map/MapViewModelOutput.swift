//
//  MapViewModelOutput.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/5/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation
import RxSwift
import CoreLocation

protocol MapViewModelOutput {
    var uploadListOfPosts: Observable<[Post]>{get}
    var longPressLocation: Observable<CLLocation>{get}
    var locationForPhoto: Observable<CLLocation>{get}
    var openDetailScreen: Observable<Post>{get}
    var navigationModeChanged: Observable<NavigationMode>{get}
    var loadView: Observable<Bool>{get}
}
