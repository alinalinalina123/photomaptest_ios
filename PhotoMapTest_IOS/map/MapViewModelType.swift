//
//  MapViewModelType.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/5/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation

protocol MapViewModelType {
    var input: MapViewModelInput {get}
    var output: MapViewModelOutput {get}
}
