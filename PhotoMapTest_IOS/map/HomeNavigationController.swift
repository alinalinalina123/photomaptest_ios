//
//  HomeNavigationController.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/2/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class HomeNavigationController {
    
    func  navigateToTakePhoto(controller: UIViewController, image: UIImage, location: CLLocation) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "PopupPhoto") as! PopupPhotoViewController
        newViewController.image = image
        newViewController.location = location
        controller.present(newViewController, animated: true, completion: nil)
    }
    
    func  navigateToFullPhoto(controller: UIViewController, post: Post, image: UIImage?) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "FullPhotoView") as! FullPhotoViewController
        newViewController.post = post
        newViewController.image = image
        controller.present(newViewController, animated: true, completion: nil)
    }
    
    func  navigateToDetailPhoto(controller: UIViewController, post: Post) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "PopupPhoto") as! PopupPhotoViewController
        newViewController.post = post
        controller.present(newViewController, animated: true, completion: nil)
    }
}
