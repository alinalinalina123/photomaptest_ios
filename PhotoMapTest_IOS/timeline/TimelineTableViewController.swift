//
//  TimelineTableViewController.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/1/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import UIKit
import RxSwift

class TimelineTableViewController: UITableViewController, UISearchBarDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
    private var posts = [(String, [Post])]()
    private let viewModel = TimeLineViewModel()
    private let bag = DisposeBag()
    private let navigation = HomeNavigationController()
    
    override func viewWillAppear(_ animated: Bool) {
        
        searchBar.delegate = self
        
        viewModel.output.searchResult
            .observeOn(MainScheduler.instance)
            .subscribe(onNext:{postList in
                self.posts.removeAll()
                self.posts = postList
                self.tableView.reloadData()
            }).disposed(by: bag)
        
        viewModel.output.sectionList
            .observeOn(MainScheduler.instance)
            .subscribe(onNext:{postList in
                self.posts.removeAll()
                self.posts = postList
                self.tableView.reloadData()
            }).disposed(by: bag)
        
         viewModel.input.viewDidLoad()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        viewModel.input.startSearch()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        viewModel.input.stopSearch()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText != "" {
            viewModel.input.startSearch()
            viewModel.input.searchByHashtag(searchString: searchText)
        } else {
            viewModel.input.stopSearch()
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return posts.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return posts[section].1.count
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return posts[section].0
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         let post = posts[indexPath.section].1[indexPath.row]
        navigation.navigateToFullPhoto(controller: self, post: post, image: nil)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
         let cellTimeline = cell as! TimelineTableViewCell
         cellTimeline.postImage.image = nil
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TimelineCell", for: indexPath) as! TimelineTableViewCell
       let post = posts[indexPath.section].1[indexPath.row]
        cell.descriptionLabel.text =  post.title
        cell.dateCategoryLabel.text = "\(post.date) \\ \(CategoryType.getCategoryStringOfPost(post: post))"
        cell.postImage.downloadedFrom(link: post.imagePath, progressBar: cell.progressBar)
        return cell
    }

}
