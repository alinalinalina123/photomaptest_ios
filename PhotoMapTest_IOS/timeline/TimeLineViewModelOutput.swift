//
//  TimeLineViewModelOutput.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/5/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation
import RxSwift
protocol TimeLineViewModelOutput {
    var timeLineList: Observable<[Post]>{get}
    var sectionList: Observable<[(String, [Post])]>{get}
    var searchResult: Observable<[(String, [Post])]>{get}
}
