//
//  TimeLineViewModel.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/5/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation
import RxSwift

class TimeLineViewModel : TimeLineViewModelType, TimeLineViewModelInput, TimeLineViewModelOutput {
    
    private var didLoad = PublishSubject<Bool>()
    func viewDidLoad(){didLoad.onNext(true)}
    
    private var searchByHashtag = PublishSubject<String>()
    func searchByHashtag(searchString: String){
        searchByHashtag.onNext(searchString)
    }
    private var stopSearchSubject = PublishSubject<Bool>()
    func stopSearch(){ stopSearchSubject.onNext(false)}
    func startSearch(){ stopSearchSubject.onNext(true)}
    
    var timeLineList: Observable<[Post]>
    var sectionList: Observable< [(String, [Post])]>
    var searchResult: Observable< [(String, [Post])]>
    
    var input: TimeLineViewModelInput { return self }
    var output: TimeLineViewModelOutput { return self }
   
    init() {
        
        timeLineList = didLoad.distinctUntilChanged().flatMap{_ in
            FBClient.client.retriveListOfPosts()
            } .map { posts in
                let categories = SelectedCategory.categories
                var natureCategory = SelectedCategory(category: CategoryType.NATURE, isSelected: false)
                var friendsCategory = SelectedCategory(category: CategoryType.FRIENDS, isSelected: false)
                var defaultCategory = SelectedCategory(category: CategoryType.DEFAULT, isSelected: false)
                for item in categories{
                    if item.categoryType.rawValue == 1 {friendsCategory = item}
                    if item.categoryType.rawValue == 2 {natureCategory = item}
                    if item.categoryType.rawValue == 3 {defaultCategory = item}
                }
                var listPosts = [Post]()
                for post in posts {
                    switch(post.category){
                    case CategoryType.DEFAULT.rawValue:
                        if defaultCategory.isSelected { listPosts.append(post)}
                    case CategoryType.FRIENDS.rawValue:
                        if friendsCategory.isSelected { listPosts.append(post)}
                    case CategoryType.NATURE.rawValue:
                        if natureCategory.isSelected { listPosts.append(post)}
                    default:
                        return posts
                    }
                }
                return listPosts
        }
    
        sectionList = timeLineList.take(1).map { list in
            return formatSections(list: list)
        }
        
        searchResult = Observable.combineLatest(searchByHashtag, timeLineList, stopSearchSubject) { search, list, stopSignal in
            
            if stopSignal {
                var filteredList = [Post]()
                let searchHashtags = search.getHashtags()
                if (searchHashtags != nil){
                    for post in list{
                        let postHastags = post.title.getHashtags()
                        for hashtag in postHastags ?? [String]() {
                            if searchHashtags!.contains(hashtag) {
                                filteredList.append(post)
                                break
                            }
                        }
                    }
                }
                return formatSections(list: filteredList)
            } else {
                return formatSections(list: list)
            }
        }
        
        
        func formatSections(list: [Post]) ->  [(String, [Post])] {
            var dictionaryPosts = [String: [Post]]()
            let dateFormatter = DateFormatter()
            let sortedList = sortPostsByDESC(posts: list)
            for item in sortedList {
                dateFormatter.dateFormat = "dd MMM yyyy hh:mm a"
                let dateSection = dateFormatter.date(from: item.date)
                dateFormatter.dateFormat = "MMMM yyyy"
                let header = dateFormatter.string(from: dateSection ?? Date())
                if dictionaryPosts[header] == nil {
                    dictionaryPosts[header] = [Post]()
                }
                dateFormatter.dateFormat = "MM-dd-YY"
                item.date = dateFormatter.string(from: dateSection ?? Date())
                dictionaryPosts[header]?.append(item)
            }
            return sortSectionsByDESC(posts: dictionaryPosts)
        }
    
        func sortPostsByDESC(posts:[Post]) -> [Post] {
            return posts.sorted(by: {$0.date>$1.date})
        }
        
        func sortSectionsByDESC(posts:[String:[Post]]) -> [(String, [Post])] {
        let dateFormatter = DateFormatter()
           dateFormatter.dateFormat = "MMM yyyy"
            let postList = posts.sorted(by: {dateFormatter.date(from: $0.key)?.compare(dateFormatter.date(from: $1.key)!) == .orderedDescending})
            return postList
        }
        
    }
}
