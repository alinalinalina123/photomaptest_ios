//
//  CategoryViewModelOutput.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/12/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation
import RxSwift

protocol CategoryViewModelOutput {
    var selectedCategories: Observable<[SelectedCategory]> {get}
    var defaultPressResult: Observable<Bool> {get}
    var naturePressResult: Observable<Bool> {get}
    var friendsPressResult: Observable<Bool> {get}
    var selectedResult: Observable<[SelectedCategory]> {get}
}
