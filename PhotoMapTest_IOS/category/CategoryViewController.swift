//
//  CategoryViewController.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/5/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import UIKit
import RxSwift

class CategoryViewController: UIViewController {

    @IBOutlet weak var natureRadioButton: RadioButton!
    @IBOutlet weak var friendsRadioButton: RadioButton!
    @IBOutlet weak var defaultRadioButton: RadioButton!
    
    var categories = SelectedCategory.categories
    let bag = DisposeBag()
    let viewModel = CategoryViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        viewModel.output.selectedCategories
            .subscribe(onNext: { categories in
                for category in categories{
                    switch(category.categoryType){
                    case CategoryType.DEFAULT: self.defaultRadioButton.isSelected = category.isSelected
                    case CategoryType.FRIENDS: self.friendsRadioButton.isSelected = category.isSelected
                    case CategoryType.NATURE: self.natureRadioButton.isSelected = category.isSelected
                    }
                }
                self.viewModel.input.defaultPressed(isSelected: !self.defaultRadioButton.isSelected)
                self.viewModel.input.friendsPressed(isSelected: !self.friendsRadioButton.isSelected)
                self.viewModel.input.naturePressed(isSelected: !self.natureRadioButton.isSelected)
            }).disposed(by: bag)
        
        viewModel.output.naturePressResult
            .subscribe(onNext: { isSelected in
                self.natureRadioButton.isSelected = isSelected
            }).disposed(by: bag)
        
        viewModel.output.friendsPressResult
            .subscribe(onNext: { isSelected in
                self.friendsRadioButton.isSelected = isSelected
            }).disposed(by: bag)
        
        viewModel.output.defaultPressResult
            .subscribe(onNext: { isSelected in
                self.defaultRadioButton.isSelected = isSelected
            }).disposed(by: bag)
        
        viewModel.output.selectedResult
            .subscribe(onNext: { categoriesResult in
                SelectedCategory.categories = categoriesResult
                self.dismiss(animated: true, completion: nil)
            }).disposed(by: bag)
        
            viewModel.input.viewLoaded(categories: categories)
        
    }
    
    @IBAction func friendsPressed(_ sender: RadioButton) {
        viewModel.input.friendsPressed(isSelected: friendsRadioButton.isSelected)
    }
    
    @IBAction func defaultPressed(_ sender: RadioButton) {
        viewModel.input.defaultPressed(isSelected: defaultRadioButton.isSelected)
    }
    
    @IBAction func naturePressed(_ sender: RadioButton) {
       viewModel.input.naturePressed(isSelected: natureRadioButton.isSelected)
    }
    
    @IBAction func donePressed(_ sender: Any) {
        viewModel.input.donePressed()
    }

}
