//
//  CategoryViewModel.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/12/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation
import RxSwift

class CategoryViewModel : CategoryViewModelType, CategoryViewModelInput, CategoryViewModelOutput {

    var input: CategoryViewModelInput { return self }
    var output: CategoryViewModelOutput { return self }
    
    //input
    private var viewLoaded = PublishSubject<[SelectedCategory]>()
    func viewLoaded(categories: [SelectedCategory]) {viewLoaded.onNext(categories)}
    
    private var defaultPressed = PublishSubject<Bool>()
    func defaultPressed(isSelected: Bool) { defaultPressed.onNext(isSelected)  }
    
    private var naturePressed = PublishSubject<Bool>()
    func naturePressed(isSelected: Bool) {naturePressed.onNext(isSelected)}
    
    private var friendsPressed = PublishSubject<Bool>()
    func friendsPressed(isSelected: Bool) { friendsPressed.onNext(isSelected)   }
    
    private var donePressedSubject = PublishSubject<Bool>()
    func donePressed(){ donePressedSubject.onNext(true)}
    
    // output
    var selectedCategories: Observable<[SelectedCategory]>
    var defaultPressResult: Observable<Bool>
    var naturePressResult: Observable<Bool>
    var friendsPressResult: Observable<Bool>
    var selectedResult: Observable<[SelectedCategory]>
    
    init() {
      
        selectedCategories = viewLoaded.map{ categories in
            return categories
        }
        
        defaultPressResult = defaultPressed.map{isSelected in
            return !isSelected
        }
        
        naturePressResult = naturePressed.map{isSelected in
            return !isSelected
        }
        
        friendsPressResult = friendsPressed.map{isSelected in
            return !isSelected
        }
        
        selectedResult = Observable.combineLatest(defaultPressResult,friendsPressResult,naturePressResult, donePressedSubject){defaultResult,friendsResult,natureResult,_ in
                return [SelectedCategory(category: CategoryType.DEFAULT, isSelected: defaultResult),SelectedCategory(category: CategoryType.FRIENDS, isSelected: friendsResult), SelectedCategory(category: CategoryType.NATURE, isSelected: natureResult)]
            }
        }
    
}
