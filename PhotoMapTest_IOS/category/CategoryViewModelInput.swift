//
//  CategoryViewModelInput.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/12/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation

protocol CategoryViewModelInput {
    func viewLoaded(categories: [SelectedCategory])
    func defaultPressed(isSelected: Bool)
    func naturePressed(isSelected: Bool)
    func friendsPressed(isSelected: Bool)
    func donePressed()
}
