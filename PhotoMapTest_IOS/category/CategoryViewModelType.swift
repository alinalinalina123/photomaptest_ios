//
//  CategoryViewModelType.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/12/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation

protocol CategoryViewModelType {
    var input: CategoryViewModelInput {get}
    var output: CategoryViewModelOutput {get}
}
