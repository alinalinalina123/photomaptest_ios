//
//  PopupPhotoViewModelOutput.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/2/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation
import RxSwift

protocol PopupPhotoViewModelOutput {
    var dateLabel: Observable<(String,Post?)>{get}
    var postAdded: Observable<Bool> {get}
    var postUpdated: Observable<Bool> {get}
    var loadScreen: Observable<Bool> {get}
}
