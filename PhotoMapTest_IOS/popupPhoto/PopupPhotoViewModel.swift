//
//  PopupPhotoViewModel.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/2/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation
import RxSwift

class PopupPhotoViewModel : PopupPhotoViewModelType, PopupPhotoViewModelInput, PopupPhotoViewModelOutput {

    var input: PopupPhotoViewModelInput { return self }
    var output: PopupPhotoViewModelOutput { return self }
  
    private var doneButtonPressed = PublishSubject<UploadPost>()
    func doneButtonPressed(image: UIImage, post: Post) {doneButtonPressed.onNext(UploadPost(post: post, image: image))}
    
    private var updatePost = PublishSubject<Post>()
    func updatePost(post: Post) {  updatePost.onNext(post)  }
    
    private var loadingView = PublishSubject<Bool>()
    func loadingView(isLoading: Bool) {loadingView.onNext(isLoading)}
    
    private var selectCategory = PublishSubject<Int>()
    func selectCategory(categoryId: Int) { selectCategory.onNext(categoryId) }
    
    private var viewDidLoad = PublishSubject<(Date,Post?)>()
    func viewDidLoad(date: Date, post: Post?) {viewDidLoad.onNext((date,post))}
    
    var dateLabel: Observable<(String, Post?)>
    var postAdded: Observable<Bool>
    var postUpdated: Observable<Bool>
    var loadScreen: Observable<Bool>
    
    init() {

        dateLabel = viewDidLoad.map{ viewInput in
            let post = viewInput.1
            let date = viewInput.0
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "MMMM"
            let mounth = dateFormatterGet.string(from: date)
             dateFormatterGet.dateFormat = "d"
            var day = dateFormatterGet.string(from: date)
            switch (day) {
                case "1" , "21" , "31":
                    day.append("st")
                case "2" , "22":
                    day.append("nd")
                case "3" ,"23":
                    day.append("rd")
                default:
                    day.append("th")
            }
            dateFormatterGet.dateFormat = "YYYY"
            let year = dateFormatterGet.string(from: date)
            dateFormatterGet.dateFormat = "h:mm a"
            let time = dateFormatterGet.string(from: date)
            return ("\(mounth) \(day), \(year) - \(time)", post)
        }
        
        postAdded = doneButtonPressed.flatMap { post in
            return FBClient.client.createPost(image: post.image, post: post.post)
        }
        
        postUpdated = updatePost.flatMap{ post in
            return FBClient.client.updatePost(post: post)
        }
        
        loadScreen = loadingView.map{isLoading in
            return isLoading
        }
    }
    
}
