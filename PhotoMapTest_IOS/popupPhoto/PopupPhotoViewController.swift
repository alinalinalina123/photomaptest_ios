//
//  PopupPhotoViewController.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/1/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import UIKit
import RxSwift
import CoreLocation

class PopupPhotoViewController: UIViewController,UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var progress: UIActivityIndicatorView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var viewPopup: UIView!
    @IBOutlet weak var titleTextField: UITextView!
    @IBOutlet weak var categoryPicker: UIPickerView!
    @IBOutlet weak var doneButton: UIButton!
    
    var post: Post!
    var location: CLLocation?
    var date: Date = Date()
    var image: UIImage!
    let viewModel = PopupPhotoViewModel()
    let bag = DisposeBag()
    let navigation = HomeNavigationController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tappedImage))
        photoImageView.addGestureRecognizer(tap)
        photoImageView.isUserInteractionEnabled = true
        
        categoryPicker.delegate = self
        categoryPicker.dataSource = self
        titleTextField.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        viewModel.output.dateLabel
            .subscribe(onNext: { viewOutput in
                 self.viewModel.input.loadingView(isLoading: false)
                 self.photoImageView.contentMode = .scaleAspectFit
                if (viewOutput.1 == nil){
                    self.dateLabel.text = viewOutput.0
                    self.photoImageView.image = self.image
                } else {
                    self.post = viewOutput.1
                    self.dateLabel.text = self.post.date
                    self.photoImageView.downloadedFrom(link: (self.post.imagePath), progressBar: self.progress)
                    self.titleTextField.text = self.post.title
                    self.doneButton.setTitle("SAVE", for: .normal)
                }
            }).disposed(by: bag)
        
        viewModel.output.loadScreen
            .subscribe(onNext: { isLoading in
                self.progress.isHidden = !isLoading
                self.viewPopup.isHidden = isLoading
            }).disposed(by: bag)
        
        viewModel.output.postAdded
            .subscribe(onNext: {
                if !$0 { AppAlert.infoAlert(title: "Error", message: "Can't add this post", selfView: self)}
                else {
                    let tabViewController = self.presentingViewController as! TabBarViewController
                    for controller in tabViewController.childViewControllers {
                        if controller is MapViewController{
                            (controller as! MapViewController).viewModel.input.didLoadView()
                        }
                    }
                }
                self.dismiss(animated: false, completion: nil)
            }).disposed(by: bag)
        
        viewModel.output.postUpdated
            .subscribe({_ in
                self.dismiss(animated: false, completion: nil)
            }).disposed(by: bag)
        
            viewModel.input.viewDidLoad(date: date, post: post)
    }
    
    @IBAction func addPost(_ sender: Any) {
        viewModel.input.loadingView(isLoading: true)
        viewModel.input.doneButtonPressed(image: image, post: createPost(post: post))
    }

    @objc func tappedImage() {
        navigation.navigateToFullPhoto(controller: self, post: createPost(post: post), image: photoImageView.image)
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
}
