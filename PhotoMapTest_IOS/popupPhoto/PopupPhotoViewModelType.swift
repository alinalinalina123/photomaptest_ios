//
//  PopupPhotoViewModelType.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/2/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation

protocol PopupPhotoViewModelType {
    var input: PopupPhotoViewModelInput {get}
    var output: PopupPhotoViewModelOutput {get}
}
