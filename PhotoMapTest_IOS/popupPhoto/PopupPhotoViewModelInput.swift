//
//  PopupPhotoViewModelInput.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/2/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation
import UIKit
protocol PopupPhotoViewModelInput {
    func doneButtonPressed(image: UIImage, post: Post)
    func updatePost(post: Post)
    func viewDidLoad (date: Date, post: Post?)
    func loadingView (isLoading: Bool)
    func selectCategory (categoryId: Int)
}
