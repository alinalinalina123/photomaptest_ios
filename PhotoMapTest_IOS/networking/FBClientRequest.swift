//
//  FBClientRequest.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 2/28/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation
import RxSwift
import UIKit
protocol FBClientRequest {
    func login(email: String, password: String) -> Observable <String?>
    func signUp(email: String, password: String) -> Observable <String?>
    func createPost(image: UIImage,post: Post) -> Observable<Bool>
    func updatePost(post: Post) -> Observable<Bool>
    func retriveListOfPosts()  -> Observable<[Post]>
}

