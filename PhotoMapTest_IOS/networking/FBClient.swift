//
//  FBHelper.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 2/28/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation
import Firebase
import FirebaseAuth
import RxSwift
import UIKit
import FirebaseDatabase
class FBClient:  FBClientRequest {
   
    static var client = FBClient()
    var ref = Database.database().reference()
    var user = Auth.auth().currentUser
    let storage = Storage.storage().reference()
   
    //SIGNALS
    private var recieveLogin = PublishSubject<Error?>()
    private var recieveUIID = PublishSubject<String?>()
    private var recieveSignUp = PublishSubject<Error?>()
    private var createdPost = PublishSubject<Bool>()
    private var retrivedListPost = PublishSubject<[Post]>()
    
    //INPUTS
    func signUp(email: String, password: String) -> Observable<String?> {
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            self.recieveSignUp.onNext(error)}
        return recieveSignUp.map{ error in return error?.localizedDescription}
    }
    
    func login(email: String, password: String) -> Observable<String?> {
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            self.recieveLogin.onNext(error)}
        return recieveLogin.map{ error in return error?.localizedDescription}
    }
    
    func logout() -> Bool {
        if Auth.auth().currentUser != nil {
            do {
                try Auth.auth().signOut()
                return true
            } catch _ as NSError {
                return false
            }
        }
        return false
    }
    
    func createPost(image: UIImage, post: Post) -> Observable<Bool> {
        let data = UIImageJPEGRepresentation(image, 100.0)
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpeg"
        let storageRef = self.storage.child("images").child(UserApp.user).child(post.date + ".jpg")
        
        storageRef.putData(data!, metadata: metadata) { (metadata, error) in
            if error != nil {
                self.createdPost.onNext(false)
            } else {
            self.createdPost.onNext(true)
            let postDictionary:[String : AnyObject] = [
                "title": post.title as AnyObject,
                "category": post.category as AnyObject,
                "date": post.date as AnyObject,
                "imagePath": metadata?.downloadURL()?.absoluteString as AnyObject,
                "latitude": post.latitude as AnyObject,
                "longtitude": post.longtitude as AnyObject
            ]
            self.ref.child("posts").child(UserApp.user).childByAutoId().setValue(postDictionary)
            }
        }
        return createdPost.map{ isFalied in return isFalied}
    }
    
    func updatePost(post: Post) -> Observable<Bool> {
        let postDictionary:[String : AnyObject] = [
            "title": post.title as AnyObject,
            "category": post.category as AnyObject]
        ref.child("posts").child(UserApp.user).child(post.key ?? "").updateChildValues(postDictionary)
        return Observable.just(true)
    }
    
    func retriveListOfPosts() -> Observable<[Post]> {
        var postsList = [Post]()
     
        ref.child("posts").child(UserApp.user).queryOrdered(byChild: "date").observeSingleEvent(of: .value, with: { (snapshot) in
            for child in snapshot.children {
                let snap = child as! DataSnapshot
                let post = snap.value as! [String:AnyObject]
                let key = snap.key
                let title = post["title"] as? String
                let latitude = post["latitude"] as! Double
                let longtitude = post["longtitude"] as! Double
                let category = post["category"] as! Int
                let image = post["imagePath"] as! String
                let date = post["date"] as! String
                print("key = \(post) ")
                postsList.append(Post(key: key, title: title ?? "", category: category, date: date, latitude: latitude, longtitude: longtitude, image: image))
            }
                self.retrivedListPost.onNext(postsList)
            })
        return retrivedListPost
    }
    
    private init() {}
    
}
