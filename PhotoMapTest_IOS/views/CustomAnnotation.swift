//
//  CustomAnnotationPin.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/9/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation
import MapKit
import CoreLocation
class CustomAnnotation :  NSObject, MKAnnotation {
    var post: Post?
    var title: String?
    var subtitle: String?
    var imagePath: String?
    var coordinate: CLLocationCoordinate2D { return CLLocationCoordinate2D(latitude: (post?.latitude)!, longitude: (post?.longtitude)!) }
    init( post: Post) {
        self.post = post
        self.title = post.title
        self.subtitle = post.date
        self.imagePath = post.imagePath
        
    }
    
}
