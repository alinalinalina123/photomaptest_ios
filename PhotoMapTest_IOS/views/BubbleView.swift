//
//  BubbleView.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/12/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class BubbleView: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 10.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var shadowView: CGFloat = 0.0 {
        didSet {
            self.layer.masksToBounds = false
            self.layer.shadowColor = UIColor.black.cgColor
            self.layer.shadowOpacity = 0.5
            self.layer.shadowOffset = CGSize(width: 1, height: 1)
            self.layer.shadowRadius = shadowView
            self.layer.shouldRasterize = true
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        backgroundColor = UIColor.clear
    }
    
    override func draw(_ rect: CGRect) {
        let size = self.bounds.size
        let h = size.height * 0.8
        
        let p1 = self.bounds.origin
        let p1_start = CGPoint(x:p1.x, y:p1.y + 30)
        let p1_end = CGPoint(x:p1.x + 30, y:p1.y)
        
        let p2 = CGPoint(x:p1.x + size.width, y:p1.y)
        let p2_start = CGPoint(x:p2.x  - 30, y:p1.y)
        let p2_end = CGPoint(x:p2.x, y:p2.y + 30)
        
        let p3 = CGPoint(x:p2.x, y:p2.y + h)
        let p3_start = CGPoint(x:p3.x, y:p3.y - 30)
        let p3_end = CGPoint(x:p3.x - 30, y:p3.y)
        
        let p4 = CGPoint(x:size.width/2 + 40, y: h)
        let p4Curve = CGPoint(x:size.width/2 + 15 , y: h)

        let p5 = CGPoint(x:size.width/2 + 8, y:size.height - 10)
        let p5Curve = CGPoint(x:size.width/2 , y: size.height + 10)
        let p5End = CGPoint(x:size.width/2 - 8, y:size.height - 10)

        let p6 = CGPoint(x:size.width/2 - 40, y: h)
        let p6Curve = CGPoint(x:size.width/2 - 15, y: h)
        
        let p7 = CGPoint(x:p1.x , y:h)
        let p7_start = CGPoint(x:p7.x + 30 , y:h)
        let p7_end = CGPoint(x:p7.x , y:h - 30)
       

       
        let path = UIBezierPath()
        path.move(to: p1_end)
        path.addLine(to: p1_end)
        path.addLine(to: p2_start)
        path.addCurve(to: p2_end, controlPoint1: p2_start, controlPoint2: p2)
        path.addLine(to: p3_start)
         path.addCurve(to: p3_end, controlPoint1: p3_start, controlPoint2: p3)
        path.addLine(to: p4)
        path.addCurve(to: p5, controlPoint1: p4, controlPoint2: p4Curve)
        path.addQuadCurve(to: p5End, controlPoint: p5Curve)
        path.addCurve(to: p6, controlPoint1: p5End, controlPoint2: p6Curve)
       path.addLine(to: p7_start)
       path.addCurve(to: p7_end, controlPoint1: p7_start, controlPoint2: p7)
        path.addLine(to: p1_start)
        path.addCurve(to: p1_end, controlPoint1: p1_start, controlPoint2: p1)
        path.close()
        UIColor.white.set()
        path.fill()
        
    }
    
}
