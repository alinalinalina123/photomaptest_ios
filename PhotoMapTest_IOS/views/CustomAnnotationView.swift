//
//  CustomAnnotationView.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/16/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation
import MapKit
private let kMapAnimationTime = 0.300
class CustomAnnotationView: MKAnnotationView {
    // data
    weak var detailDelegate: DetailMapViewDelegate?
    weak var customCalloutView: CustomCallOutView?
    override var annotation: MKAnnotation? {
        willSet { customCalloutView?.removeFromSuperview() }
    }
    
    // MARK: - life cycle
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        self.canShowCallout = false
        let annotationCustom = annotation as! CustomAnnotation
        switch ( annotationCustom.post?.category ?? 0){
                            case 1: self.image = #imageLiteral(resourceName: "friends")
                            case 2: self.image = #imageLiteral(resourceName: "nature")
                            case 3:  self.image = #imageLiteral(resourceName: "default")
                           default: self.image = #imageLiteral(resourceName: "default")
                       }
    
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.canShowCallout = false
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if selected {
            self.customCalloutView?.removeFromSuperview()
            
            if let newCustomCalloutView = loadDetailMapView() {
        
                newCustomCalloutView.frame.origin.x -= newCustomCalloutView.frame.width / 2.0 - (self.frame.width / 2.0)
                newCustomCalloutView.frame.origin.y -= newCustomCalloutView.frame.height
                
        
                self.addSubview(newCustomCalloutView)
                self.customCalloutView = newCustomCalloutView
                
        
                if animated {
                    self.customCalloutView!.alpha = 0.0
                    UIView.animate(withDuration: kMapAnimationTime, animations: {
                        self.customCalloutView!.alpha = 1.0
                    })
                }
            }
        } else {
           self.customCalloutView!.removeFromSuperview()
        }
    }

    
    func loadDetailMapView() -> CustomCallOutView? {
        
        if let views = Bundle.main.loadNibNamed("CustomCallOutView", owner: nil, options: nil) as? [CustomCallOutView], views.count > 0 {
            let detailMapView = views.first!
            detailMapView.delegate = self.detailDelegate
            if let annotation = annotation as? CustomAnnotation {
                let post = annotation.post
                detailMapView.configurePost(post: post!)
            }
            return detailMapView
        }
        return nil
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.customCalloutView?.removeFromSuperview()
    }
    
    
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
       
        if let parentHitView = super.hitTest(point, with: event) { return parentHitView }
        else {
            if customCalloutView != nil {
                return customCalloutView!.hitTest(convert(point, to: customCalloutView!), with: event)
            } else { return nil }
        }
    }
}
