//
//  CustomCallOutView.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/12/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation
import UIKit
import MapKit

protocol DetailMapViewDelegate: class {
    func detailsRequestedForPost(post: Post)
}

class CustomCallOutView: UIView {
    var post: Post!
   
    weak var delegate: DetailMapViewDelegate?
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var photoImage: UIImageView!
    @IBOutlet weak var popupButton: UIButton!
    
    override func awakeFromNib() {
         super.awakeFromNib()
         photoImage.setShadow()
    }
    
    func configurePost(post: Post) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy hh:mm a"
        let date = dateFormatter.date(from: post.date)
        dateFormatter.dateFormat = "MM-dd-YY"
        self.post = post
        titleLabel.text = self.post.title
        dateLabel.text = dateFormatter.string(from: date ?? Date())
        photoImage.downloadedFrom(link: self.post.imagePath, progressBar: nil)
    }
   
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if popupButton.frame.contains(point) {
            delegate?.detailsRequestedForPost(post: post)
        }
        return popupButton.hitTest(convert(point, to: popupButton), with: event)
    }
    
}


