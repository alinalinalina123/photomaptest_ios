//
//  NavigationMode.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/2/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation

enum NavigationMode {
    case FOLLOW
    case DISCOVER
}
class SelectedNavigation {
    var navigationMode: NavigationMode
    static var modeNavigation = NavigationMode.FOLLOW
   
    init(mode:NavigationMode){
        self.navigationMode = mode
    }
}
