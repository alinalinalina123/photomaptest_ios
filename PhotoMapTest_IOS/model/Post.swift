//
//  Post.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 2/28/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation
import UIKit
class Post {

    var key: String?
    var title: String
    var imagePath: String
    var date: String
    var category: Int
    var latitude: Double
    var longtitude: Double
    
    init(title: String, category: Int, date: String, latitude: Double, longtitude: Double) {
        self.title = title
        self.imagePath = ""
        self.date = date
        self.category = category
        self.latitude = latitude
        self.longtitude = longtitude
        
    }
    
    init(title: String, category: Int, date: String, latitude: Double, longtitude: Double, image: String) {
        self.title = title
        self.imagePath = ""
        self.date = date
        self.category = category
        self.latitude = latitude
        self.longtitude = longtitude
        self.imagePath = image
    }
    
    init(key:String, title: String, category: Int, date: String, latitude: Double, longtitude: Double, image: String) {
        self.title = title
        self.imagePath = ""
        self.date = date
        self.category = category
        self.latitude = latitude
        self.longtitude = longtitude
        self.imagePath = image
        self.key = key
    }
    
    
}

class PostValue {
    var posts: [Post]!
    var key: String!
}

class UploadPost {
    var post: Post
    let image: UIImage
    init(post: Post, image: UIImage) {
        self.image = image
        self.post = post
    }
}
