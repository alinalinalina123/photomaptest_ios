//
//  CategoryType.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/5/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation

enum CategoryType: Int{
    case FRIENDS = 1
    case NATURE = 2
    case DEFAULT = 3
}



class SelectedCategory {
    var categoryType: CategoryType
    var isSelected: Bool
    
    static var categories = [SelectedCategory](arrayLiteral: SelectedCategory(category: CategoryType.DEFAULT, isSelected: true),SelectedCategory(category: CategoryType.FRIENDS, isSelected: true), SelectedCategory(category: CategoryType.NATURE, isSelected: true))
    
    init(category: CategoryType, isSelected: Bool){
        self.categoryType = category
        self.isSelected = isSelected
    }
}
