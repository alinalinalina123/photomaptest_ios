//
//  FullPhotoViewModelInput.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/12/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation
import UIKit
protocol FullPhotoViewModelInput {
    func oneTapped(isHeaderVisible: Bool)
    func viewLoaded(post: Post)
    func doubleTapped(zoomScale: CGFloat)
}
