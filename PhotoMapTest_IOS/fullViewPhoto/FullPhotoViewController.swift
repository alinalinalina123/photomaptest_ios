//
//  FullPhotoViewController.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/7/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import UIKit
import RxSwift

class FullPhotoViewController: UIViewController {

    @IBOutlet weak var progressBar: UIActivityIndicatorView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imagePhoto: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var footer: UIView!
    @IBOutlet weak var header: UIView!
    @IBOutlet weak var scroll: UIScrollView!
    
    let bag = DisposeBag()
    var post: Post?
    var image: UIImage?
    var gradient: CAGradientLayer!
    private let viewModel = FullPhotoViewModel()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.output.oneTapResult
            .subscribe(onNext: { hideStatus in
                self.header.isHidden = hideStatus
                self.footer.isHidden = hideStatus
            }).disposed(by: bag)
        
        viewModel.output.imagePath
            .subscribe(onNext:{ imagePath in
                if (self.image != nil) {
                    self.imagePhoto.image = self.image
                    self.progressBar.isHidden = true
                } else {
                    self.imagePhoto.downloadedFrom(link: imagePath, progressBar: self.progressBar)
                }
            }).disposed(by: bag)
        
        viewModel.output.datePost
            .subscribe(onNext: { date in
                self.dateLabel.text = date
            }).disposed(by: bag)
        
        viewModel.output.titlePost
            .subscribe(onNext: { title in
                self.titleLabel.text = title
            }).disposed(by: bag)
        
        viewModel.output.scaleImage
            .subscribe(onNext:{ scaleZoom in
                self.scroll.setZoomScale(scaleZoom, animated: true)
            }).disposed(by: bag)
        
        
        viewModel.input.viewLoaded(post: post!)
    }

    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imagePhoto
    }
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func oneTap(_ sender: UITapGestureRecognizer) {
        viewModel.oneTapped(isHeaderVisible: header.isHidden)
    }
    
    @IBAction func doubleTap(_ sender: UITapGestureRecognizer) {
        viewModel.input.doubleTapped(zoomScale: scroll.zoomScale)
    }
    
    
}
