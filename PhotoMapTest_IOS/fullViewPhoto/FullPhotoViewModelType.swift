//
//  FullPhotoViewModelType.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/12/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation

protocol FullPhotoViewModelType {
    var input: FullPhotoViewModelInput {get}
    var output: FullPhotoViewModelOutput {get}
}
