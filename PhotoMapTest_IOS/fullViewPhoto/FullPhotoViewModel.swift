//
//  FullPhotoViewModel.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/12/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation
import RxSwift
import UIKit

class FullPhotoViewModel : FullPhotoViewModelType, FullPhotoViewModelInput, FullPhotoViewModelOutput {
    
    private var oneTapped = PublishSubject<Bool>()
    func oneTapped(isHeaderVisible: Bool) {oneTapped.onNext(isHeaderVisible)}
    
    private var doubleTappedSubject = PublishSubject<CGFloat>()
    func doubleTapped(zoomScale: CGFloat) {doubleTappedSubject.onNext(zoomScale)}
    
    private var viewLoaded = PublishSubject<Post>()
    func viewLoaded(post: Post) {viewLoaded.onNext(post)}
    
    var oneTapResult: Observable<Bool>
    var titlePost: Observable<String>
    var datePost: Observable<String>
    var imagePath: Observable<String>
    var scaleImage: Observable<CGFloat>
    
    var input: FullPhotoViewModelInput { return self }
    var output: FullPhotoViewModelOutput { return self }
    
    init() {
        
        oneTapResult = oneTapped.map{ isHidden in
                return  !isHidden
        }
        
        titlePost = viewLoaded.map{ post in
            return post.title
        }
        
        datePost = viewLoaded.map{ post in
            return post.date
        }
        
        imagePath = viewLoaded.map{ post in
            return post.imagePath
        }
        
        scaleImage = doubleTappedSubject.map{ zoomScale in
            if (zoomScale < 1.5) {
                return 3.0
            } else {
                return 1.0
            }
        }
        
        
    }
}
