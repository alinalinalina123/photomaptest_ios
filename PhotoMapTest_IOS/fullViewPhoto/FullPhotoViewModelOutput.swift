//
//  FullPhotoViewModelOutput.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/12/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation
import RxSwift
import UIKit

protocol FullPhotoViewModelOutput {
    var oneTapResult: Observable<Bool> {get}
    var titlePost: Observable<String> {get}
    var datePost: Observable<String> {get}
    var imagePath: Observable<String> {get}
    var scaleImage: Observable<CGFloat> {get}
}
