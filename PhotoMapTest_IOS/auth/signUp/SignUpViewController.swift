//
//  SignUpViewController.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 2/28/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import UIKit
import RxSwift
class SignUpViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    let bag = DisposeBag()
    let viewModel = SignUpViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailTextField.delegate = self
        passwordTextField.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        viewModel.output.signedIn
            .subscribe(onNext: {
                if $0 != nil {AppAlert.infoAlert(title: "Sign up failed", message: $0 ?? "Unknown", selfView: self)}
            }).disposed(by: bag)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    @IBAction func signUpUser(_ sender: Any) {
        viewModel.input.signUpButtonPressed(email: emailTextField.text ?? "", password: passwordTextField.text ?? "")
    }
    
}
