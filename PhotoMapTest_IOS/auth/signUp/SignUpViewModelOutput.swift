//
//  SignUpViewModelOutput.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 2/28/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation
import RxSwift

protocol SignUpViewModelOutput {
    var signedIn: Observable<String?> {get}
}
