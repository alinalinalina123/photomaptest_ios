//
//  SignUpViewModel.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 2/28/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation
import RxSwift

class SignUpViewModel : SignUpViewModelType, SignUpViewModelInput, SignUpViewModelOutput {
    
    private var signUpButtonPressed = PublishSubject<UserApp>()
    func signUpButtonPressed(email: String, password: String) { signUpButtonPressed.onNext(UserApp(email: email, password: password))}
    
    var signedIn: Observable<String?>
    
    
    var input: SignUpViewModelInput { return self }
    var output: SignUpViewModelOutput { return self }
    
    init() {
        
        signedIn = signUpButtonPressed.asObservable().flatMap {
          FBClient.client.signUp(email: $0.email, password: $0.password).asObservable()
            }.map { $0 }
        
    }
    
}
