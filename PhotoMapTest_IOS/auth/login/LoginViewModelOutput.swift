//
//  LoginViewModelOutput.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/1/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation
import RxSwift


protocol LoginViewModelOutput {
    var logedIn: Observable<String?> {get}
   
}
