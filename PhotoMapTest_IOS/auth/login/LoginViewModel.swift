//
//  LoginViewModel.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/1/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation
import RxSwift

class LoginViewModel : LoginViewModelType, LoginViewModelInput, LoginViewModelOutput {
    
    private var loginButtonPressed = PublishSubject<UserApp>()
    func loginButtonPressed(email: String, password: String) { loginButtonPressed.onNext(UserApp(email: email, password: password))}
    
    var logedIn: Observable<String?>
    
    
    var input: LoginViewModelInput { return self }
    var output: LoginViewModelOutput { return self }
    
    init() {
        
        logedIn = loginButtonPressed.asObservable().flatMap {
            FBClient.client.login(email: $0.email, password: $0.password).asObservable()
            }.map { $0 }
        
    }
    
}
