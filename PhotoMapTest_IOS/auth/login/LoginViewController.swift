//
//  ViewController.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 2/27/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import UIKit
import RxSwift
import FirebaseAuth

class LoginViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    let bag = DisposeBag()
    let viewModel = LoginViewModel()
    let navigation = AuthNavigationController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailTextField.delegate = self
        passwordTextField.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        viewModel.output.logedIn
            .subscribe(onNext: { if $0 != nil { AppAlert.infoAlert(title: "Login failed", message: $0 ?? "Unknown", selfView: self)
                } else {
                self.emailTextField.text = ""
                self.passwordTextField.text = ""
                self.navigation.navigateToMain(controller: self) }
            }).disposed(by: bag)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }

    override func viewDidAppear(_ animated: Bool) {
        if Auth.auth().currentUser != nil {
            self.navigation.navigateToMain(controller: self)
        }
    }
    
    @IBAction func loginUser(_ sender: Any) {
        viewModel.input.loginButtonPressed(email: emailTextField.text ?? "", password: passwordTextField.text ?? "")
    }
    
}

