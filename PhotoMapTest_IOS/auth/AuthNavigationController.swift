//
//  AuthNavigationController.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/1/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation
import UIKit

class AuthNavigationController {
    func  navigateToMain(controller: UIViewController) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "TabBarController") as! TabBarViewController
        controller.present(newViewController, animated: true, completion: nil)
    }
    
    func  navigateToLogin(controller: UIViewController) {
      controller.dismiss(animated: false, completion: nil)
//        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let newViewController = storyBoard.instantiateViewController(withIdentifier: "LoginView") as! LoginViewController
//        controller.present(newViewController, animated: true, completion: nil)
       
    }
}
