//
//  LogoutViewController.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/1/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import UIKit
import RxSwift

class LogoutViewController: UIViewController {

    private let viewModel = LogoutViewModel()
    private let bag = DisposeBag()
    private let navigation = AuthNavigationController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.output.logoutResult
            .subscribe(onNext:{ result in
                if result {
                    self.navigation.navigateToLogin(controller: self)
                }
            }).disposed(by: bag)
        
    }
    
    @IBAction func logout(_ sender: Any) {
        AppAlert.logoutAlert(selfView: self)
            .subscribe(onNext:{ result in
                self.viewModel.input.logoutButtonPressed(isConfirmed: result)
            }).disposed(by: bag)
    }
    
}
