//
//  LogoutViewModelOutput.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/14/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation
import RxSwift
protocol LogoutViewModelOutput {
    var logoutResult: Observable<Bool>{get}
}
