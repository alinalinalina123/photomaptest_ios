//
//  LogoutViewModel.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/14/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation
import RxSwift

class LogoutViewModel : LogoutViewModelType, LogoutViewModelInput, LogoutViewModelOutput {
    
    private var logoutButtonPressed = PublishSubject<Bool>()
    func logoutButtonPressed(isConfirmed: Bool) { logoutButtonPressed.onNext(isConfirmed)}
    
    var logoutResult: Observable<Bool>
    
    var input: LogoutViewModelInput { return self }
    var output: LogoutViewModelOutput { return self }
    
    init() {
        
        logoutResult = logoutButtonPressed.map{ isConfirmed in
            
            var logoutStatus = false
            if isConfirmed {
                logoutStatus = FBClient.client.logout()
            } 
            return logoutStatus
        }
    }
    
}
