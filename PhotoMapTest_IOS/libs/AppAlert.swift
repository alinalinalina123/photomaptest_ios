//
//  AppAlert.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 2/28/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import CoreLocation

class AppAlert {
    
    static func infoAlert(title:String, message: String, selfView: UIViewController) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        selfView.present(alert, animated: true)
    }

    static func logoutAlert(selfView: UIViewController) -> Observable<Bool> {
        let optionMenu = UIAlertController(title: "Logout", message:"Are you sure?", preferredStyle: .alert)
        let desicion = PublishSubject<Bool>()
        let saveAction = UIAlertAction(title: "Logout", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
           desicion.onNext(true)
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
           desicion.onNext(false)
        })
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        selfView.present(optionMenu, animated: true, completion: nil)
        return desicion
    }
    
    static func actionSheetPhoto(selfView: UIViewController, userLocation:  CLLocation) {
        let optionMenu = UIAlertController(title: nil, message:nil, preferredStyle: .actionSheet)
        let deleteAction = UIAlertAction(title: "Take a Picture", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            (selfView as? MapViewController)?.takePhoto(location: userLocation)
        })
        let saveAction = UIAlertAction(title: "Choose From Library", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            (selfView as? MapViewController)?.getFromLibrary(location: userLocation)
        })

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        selfView.present(optionMenu, animated: true, completion: nil)
    }
}
