//
//  UIImageExtension.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/6/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

extension UIImageView {
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleToFill, progressBar: UIActivityIndicatorView? )  {
        guard let url = URL(string: link) else { return  }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
                if (progressBar != nil) {
                    progressBar?.isHidden = true
                }
            }
        }.resume()
    }
}

extension FullPhotoViewController {
  
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        gradient.frame = header.bounds
    }
    
    func applyGradient() -> Void {
        gradient = CAGradientLayer()
        gradient.frame = header.bounds
        gradient.colors = [UIColor.clear.cgColor, UIColor.black.cgColor, UIColor.black.cgColor, UIColor.clear.cgColor]
        gradient.locations = [0, 0, 0.3, 1]
        header.layer.mask = gradient
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        self.scroll.delegate = self
        self.scroll.minimumZoomScale = 1.0
        self.scroll.maximumZoomScale = 3.0
        applyGradient()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(doubleTap))
        tap.numberOfTapsRequired = 2
        tap.numberOfTouchesRequired = 1
        view.addGestureRecognizer(tap)
        
        let tapOne = UITapGestureRecognizer(target: self, action: #selector(oneTap))
        tapOne.numberOfTapsRequired = 1
        tapOne.numberOfTouchesRequired = 1
        view.addGestureRecognizer(tapOne)
       
    }
}


extension UIView {
    func setShadow(){
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.layer.shadowRadius = 5
        self.layer.shouldRasterize = true
    }
}
