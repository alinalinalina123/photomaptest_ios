//
//  TableViewExtensions.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/1/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation
import UIKit

extension PopupPhotoViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    override func viewWillAppear(_ animated: Bool) {
        viewPopup.setShadow()
        photoImageView.setShadow()
    }
    
    func willDismissModalController () {
        
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let myView = UIView(frame: CGRect(x: 0, y: 0, width: pickerView.bounds.width - 10, height: 20))
        let myImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        var rowString = String()
        switch row {
        case 0:
            rowString = "FRIENDS"
            myImageView.image = #imageLiteral(resourceName: "friends")
        case 1:
            rowString = "NATURE"
            myImageView.image = #imageLiteral(resourceName: "nature")
        case 2:
            rowString = "DEFAULT"
            myImageView.image = #imageLiteral(resourceName: "default")
        case 3:
            break
        default:
            rowString = "Error: too many rows"
            myImageView.image = nil
        }
        let myLabel = UILabel(frame: CGRect(x: 30, y: 0, width: pickerView.bounds.width - 10, height: 20))
        myLabel.text = rowString
        myLabel.textColor = UIColor.darkGray
        
        myView.addSubview(myLabel)
        myView.addSubview(myImageView)
        return myView
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 3
    }
    
    func createPost( post:Post?) -> Post {
        var postToSent = post
        if (postToSent != nil){
            postToSent?.category = categoryPicker.selectedRow(inComponent: 0) + 1
            postToSent?.title = titleTextField.text
        } else {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd MMM yyyy hh:mm a"
            postToSent = Post(
                title: titleTextField.text,
                category: categoryPicker.selectedRow(inComponent: 0) + 1,
                date: dateFormatter.string(from: date),
                latitude:(location?.coordinate.latitude) ?? self.post.latitude,
                longtitude: (location?.coordinate.longitude) ??  self.post.longtitude)
        }
        return postToSent!
    }
    
}
