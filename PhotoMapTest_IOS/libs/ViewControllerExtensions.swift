//
//  ViewControllerExtensions.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/1/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation

import UIKit

extension UIViewController: UITextFieldDelegate, UITextViewDelegate {
    
    func moveTextField( moveDistance: Int, up: Bool) {
        let moveDuration = 0.3
        let movement: CGFloat = CGFloat(up ? moveDistance : -moveDistance)
        UIView.beginAnimations("animateTextField", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(moveDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }
    
    public func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if self.view.frame.minY == 0 {
            moveTextField( moveDistance: -150, up: true)
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.minY == -150 {
            moveTextField( moveDistance: -150, up: false)
        }
    }

    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.view.endEditing(true)
        return true
    }

}
