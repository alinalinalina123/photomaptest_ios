//
//  UIViewExtensions.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/7/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation
import UIKit

extension TimelineTableViewController {
    
   override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        let headerLabel = UILabel(frame: CGRect(x: 30, y: 0, width:
            tableView.bounds.size.width, height: tableView.bounds.size.height))
        if #available(iOS 8.2, *) {
            headerLabel.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.thin )
        }
        headerLabel.textColor = UIColor.gray
        headerLabel.text = self.tableView(self.tableView, titleForHeaderInSection: section)
        headerLabel.sizeToFit()
        headerView.addSubview(headerLabel)
        return headerView
    }
}

extension CategoryType {
    static func getCategoryStringOfPost(post: Post) -> String {
        var category: String
        switch post.category {
        case 1:
            category = "FRIENDS"
        case 2:
            category = "NATURE"
        default:
             category = "DEFAULT"
        }
        return category
    }
}
