//
//  MapExtensions.swift
//  PhotoMapTest_IOS
//
//  Created by mac-242 on 3/2/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

import Foundation
import MapKit
import CoreLocation

extension MapViewController: CLLocationManagerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, MKMapViewDelegate{
    
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        let status  = CLLocationManager.authorizationStatus()
        if status == .notDetermined {
            locationManager.requestWhenInUseAuthorization()
            return
        }
        
        if status == .denied || status == .restricted {
            let alert = UIAlertController(title: "Location Services Disabled", message: "Please enable Location Services in Settings", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(okAction)
            present(alert, animated: true, completion: nil)
            return
        }

        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        
    }
    
    func changeModeNavigation () {
        var color: UIColor
        if( SelectedNavigation.modeNavigation == NavigationMode.FOLLOW) {
            color = UIColor(rgb: 0x368edf)
             mapView.userTrackingMode = .follow
        } else {
            color = UIColor(rgb: 0x5F5C5C)
             mapView.userTrackingMode = .none
        }
        let origImage = UIImage(named: "navigation_icon")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        modeButton.setImage(tintedImage, for: .normal)
        modeButton.tintColor = color
    }
    
   public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {}
    
   public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {}
    
   public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }
    
    
    func getFromLibrary(location: CLLocation){
        self.location = location
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(picker, animated: true, completion: nil)
    }
    
    func takePhoto(location: CLLocation){
          self.location = location
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.delegate = self
            picker.sourceType = .camera;
            picker.allowsEditing = false
            present(picker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let navigation = HomeNavigationController()
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        dismiss(animated:true, completion: nil)
        navigation.navigateToTakePhoto(controller: self, image: chosenImage, location: location!)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation { return nil }
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "pin")
        annotationView = CustomAnnotationView(annotation: annotation, reuseIdentifier: "pin")
        (annotationView as! CustomAnnotationView).detailDelegate = self
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
         mapView.setCenter((view.annotation?.coordinate)!, animated: true)
    }
    
    func initMapView() {
        progressBar.isHidden = true
        picker.delegate = self
        mapView.delegate = self
        determineMyCurrentLocation()
        mapView.userTrackingMode = .follow
        mapView.showsUserLocation = true
        let rotateGesture = UIRotationGestureRecognizer(target: self, action: #selector(MapViewController.rotateMap (_:)))
        mapView.addGestureRecognizer(rotateGesture)
        rotateGesture.delegate = self
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(MapViewController.dragMap (_:)))
        mapView.addGestureRecognizer(panGesture)
        panGesture.delegate = self
    }
}
